﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.AccessControl;
using FileSystemVisitor;
using FileSystemVisitor.Implementation;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;


namespace Tests
{
    [TestFixture]
    public class FileSystemVisitorTests
    {
        private readonly string testFolderPath = @"D:\Learn\Mentoring\Tasks\NET.Mentoring.Freidlina.01\Tests\test";
        private static readonly List<string> filters;
        private static readonly FilterProvider _filterProvider;
        static FileSystemVisitorTests()
        {
            _filterProvider = new FilterProvider();
            filters = _filterProvider.GetFiltersList().ToList();
        }

        public static IEnumerable<TestCaseData> TestCasesForDirSearch
        {
            get
            {
                yield return new TestCaseData(null,"").Returns(20);
                yield return new TestCaseData(filters[1], "txt").Returns(6);
                yield return new TestCaseData(filters[1], "bmp").Returns(4);
                yield return new TestCaseData(filters[1], "docx").Returns(2);
                yield return new TestCaseData(filters[1], "").Returns(0);
                yield return new TestCaseData(filters[0], "2017").Returns(20);
                yield return new TestCaseData(filters[0], "2016").Returns(0);
                yield return new TestCaseData(filters[3], "").Returns(12);
                yield return new TestCaseData(filters[3], "file1").Returns(7);
                yield return new TestCaseData(filters[2], "").Returns(8);
                yield return new TestCaseData(filters[2], "folder1").Returns(3);
            }
        }
        [Test, TestCaseSource(nameof(TestCasesForDirSearch))]
        public int TestDirSearch(string filter, string filterPattern)
        {
            var fsVisitor = new FileSystemVisitor.FileSystemVisitor(_filterProvider.GetFilter(filter,filterPattern),new FileSystemReader());
            var res = fsVisitor.GetFilteredFsObjects(testFolderPath).Where(x=>!string.IsNullOrEmpty(x)).ToArray();
            foreach (var r in res)
            {
                Trace.WriteLine(r);
            }
            return res.Length;
        }
    }
}
