﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using FileSystemVisitor.Interface;
using FileSystemVisitor.Model;

namespace FileSystemVisitor
{
    public class FilterProvider
    {
        private string selectedFilterName = "";
        private Func<FsObject, FsObject> filter = s => s;
        private readonly List<IFilter> availableFilters = new List<IFilter>();

        public FilterProvider()
        {
            var type = typeof(IFilter);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && p.IsClass && !p.IsAbstract && p.IsPublic);
            availableFilters = types.Select(f=> (IFilter) Activator.CreateInstance(f)).ToList();
        }

        public IEnumerable<string> GetFiltersList()
        {
            return availableFilters.Select(f => f.FilterName);
        }

        public IFilter GetFilter(string filterName, string pattern)
        {
            var filterObject = availableFilters.FirstOrDefault(f => f.FilterName == filterName);
            if (filterObject == null) return null;
            filter = filterObject.Filter;
            filterObject.FilterPattern = pattern;
            return filterObject;
        }

        class DefaultFilter: IFilter
        {
            public string FilterName { get; } = "Default";
            public string FilterPattern { get; set; } = "";
            public FsObject Filter(FsObject fileObj) => fileObj;
        }

        public IFilter GetDefaultFilter() => new DefaultFilter();

        public FsObject Filter(FsObject path)
        {
            return filter(path);
        }

    }
}
