﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Interface;
using FileSystemVisitor.Model;

namespace FileSystemVisitor
{
    public class FileSystemVisitor
    {
        public event EventHandler<FsVisitorEventArgs> Start;
        public event EventHandler<FsVisitorEventArgs> End;

        public event EventHandler<FsVisitorEventArgs> FileFinded;
        public event EventHandler<FsVisitorEventArgs> DirectoryFinded;
        public event EventHandler<FsVisitorEventArgs> FilteredFileFinded;
        public event EventHandler<FsVisitorEventArgs> FilteredDirectoryFinded;

        private readonly IFilter _filter;
        private readonly IIoReader _reader;
        private readonly string _filterPattern;

        public FileSystemVisitor(IFilter filter, IIoReader reader)
        {
            _filter = filter;
            _reader = reader;
        }

        public FileSystemVisitor(IFilter filter, IIoReader reader, string filterPattern): this(filter,reader)
        {
            _filterPattern = filterPattern;
        }

        public IEnumerable<string> GetFilteredFsObjects(string parentPath)
        {
            foreach (var fileObj in _reader.GetFileObjects(parentPath))
            {
                if (fileObj.IsFile) OnFileFinded(new FsVisitorEventArgs("File finded"));
                else OnDirectoryFinded(new FsVisitorEventArgs("Directory finded"));
                var filteredFileObj = _filter.Filter(fileObj).FullPath;
                if (filteredFileObj == null) yield return null;
                if (fileObj.IsFile) OnFilteredFileFinded(new FsVisitorEventArgs("Filtered file finded"));
                else OnFilteredDirectoryFinded(new FsVisitorEventArgs("Filtered directory finded"));
                yield return filteredFileObj;
            }
        }

        #region Event Handlers
        protected virtual void OnStart(FsVisitorEventArgs e)
        {
            SendMessage(Start, e);
        }
        protected virtual void OnEnd(FsVisitorEventArgs e)
        {
            SendMessage(End, e);
        }
        protected virtual void OnFileFinded(FsVisitorEventArgs e)
        {
            SendMessage(FileFinded, e);
        }
        protected virtual void OnDirectoryFinded(FsVisitorEventArgs e)
        {
            SendMessage(DirectoryFinded, e);
        }
        protected virtual void OnFilteredFileFinded(FsVisitorEventArgs e)
        {
            SendMessage(FilteredFileFinded, e);
        }
        protected virtual void OnFilteredDirectoryFinded(FsVisitorEventArgs e)
        {
            SendMessage(FilteredDirectoryFinded, e);
        }
        private void SendMessage(EventHandler<FsVisitorEventArgs> handler, FsVisitorEventArgs e)
        {
            if (handler == null) return;
            e.Message += $" at {DateTime.Now.ToString(CultureInfo.InvariantCulture)}";
            handler(this, e);
        }
#endregion
    }
}

public class FsVisitorEventArgs : EventArgs
{
    public FsVisitorEventArgs(string s)
    {
        Message = s;
    }

    public string Message { get; set; }
}


