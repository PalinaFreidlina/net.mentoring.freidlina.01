﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Implementation;
using FileSystemVisitor.Model;

namespace FileSystemVisitor.Interface
{
    public interface IFilter
    {
        string FilterName { get; }
        string FilterPattern { get; set; }
        FsObject Filter(FsObject fileObj);

    }
}
