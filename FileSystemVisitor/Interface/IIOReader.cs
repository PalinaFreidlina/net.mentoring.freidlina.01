﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Implementation;
using FileSystemVisitor.Model;

namespace FileSystemVisitor.Interface
{
    public interface IIoReader
    {
        IEnumerable<FsObject> GetFileObjects(string parentPath);
    }
}
