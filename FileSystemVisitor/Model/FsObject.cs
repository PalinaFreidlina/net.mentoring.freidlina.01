﻿using System;
using System.IO;

namespace FileSystemVisitor.Model
{
    public class FsObject
    {
        public string FullPath { get; }
        public string Name { get; }
        public bool IsFile { get; }
        public string FullDirectoryPath { get; }
        public string Extention { get; }
        public DateTime CreatedDate { get; }
        public string LastDirectoryName { get; }

        public FsObject(string path)
        {
            FullPath = Path.GetFullPath(path);
            Name = Path.GetFileName(path);
            IsFile = !File.GetAttributes(path).HasFlag(FileAttributes.Directory);
            FullDirectoryPath = Path.GetDirectoryName(path);
            Extention = Path.GetExtension(path).Replace(".", "");
            CreatedDate = File.GetCreationTime(path);
            LastDirectoryName =
                FullDirectoryPath?.Substring(FullDirectoryPath.LastIndexOf("\\", StringComparison.Ordinal)+1);
        }
    }
}
