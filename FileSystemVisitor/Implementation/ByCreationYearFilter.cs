﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Interface;
using FileSystemVisitor.Model;

namespace FileSystemVisitor.Implementation
{
    public class ByCreationYearFilter: IFilter
    {
        public string FilterName { get; } = "ByCreationYear";
        public string FilterPattern { get; set; } = "";
        
        public FsObject Filter(FsObject fileObj)
        {
            if (fileObj == null) return null;
            var year = fileObj.CreatedDate.Year;
            return int.Parse(FilterPattern) == year ? fileObj : null;
        }
    }
}
