﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Interface;
using FileSystemVisitor.Model;

namespace FileSystemVisitor.Implementation
{
    public class ByExtentionFilter: IFilter
    {
        public string FilterName { get; } = "ByExtention";
        public string FilterPattern { get; set; } = "";

        public FsObject Filter(FsObject fileObj)
        {
            var pathExt = fileObj.Extention;
            if (string.IsNullOrEmpty(pathExt)) return null;
            pathExt = pathExt.Replace(".", "");
            return pathExt.Equals(FilterPattern) ? fileObj : null;
        }
    }
}
