﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Interface;
using FileSystemVisitor.Model;

namespace FileSystemVisitor.Implementation
{
    public class OnlyFilesFilter: IFilter
    {
        public string FilterName { get; } = "OnlyFiles";
        public string FilterPattern { get; set; }
        public FsObject Filter(FsObject fileObj)
        {
            if (fileObj == null || !fileObj.IsFile) return null;
            return !string.IsNullOrEmpty(FilterPattern)
                ? fileObj.Name.Contains(FilterPattern)
                    ? fileObj
                    : null
                : fileObj;
        }
    }
}
