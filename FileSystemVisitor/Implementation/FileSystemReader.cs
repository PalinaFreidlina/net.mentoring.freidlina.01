﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemVisitor.Interface;
using FileSystemVisitor.Model;

namespace FileSystemVisitor.Implementation
{
    public class FileSystemReader: IIoReader
    {
        public IEnumerable<FsObject> GetFileObjects(string parentPath)
        {
            var fullParentPath = Path.GetFullPath(parentPath);
            if (!Directory.Exists(fullParentPath)) throw new DirectoryNotFoundException();
            return Directory.EnumerateFiles(fullParentPath).Select(f => new FsObject(f))
                .Concat(DirSearchRecursive(fullParentPath));
        }

        private IEnumerable<FsObject> DirSearchRecursive(string path)
        {
            yield return new FsObject(path);
            foreach (var d in Directory.EnumerateDirectories(path))
            {
                foreach (var dd in DirSearchRecursive(d))
                {
                    yield return new FsObject(dd.FullPath);
                }
                foreach (var f in Directory.EnumerateFiles(d))
                {
                    yield return new FsObject(f);
                }
            }
        }
    }
}
