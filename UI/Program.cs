﻿using System;
using System.Linq;
using FileSystemVisitor;
using FileSystemVisitor.Implementation;
using FileSystemVisitor.Interface;

namespace UI
{
    class Program
    {
        private const string DefaultPath = @"D:\Learn";
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Input search path or press Enter for using default...");
                var path = Console.ReadLine();
                
                path = string.IsNullOrEmpty(path) ? DefaultPath : path;
                var visitor = new FileSystemVisitor.FileSystemVisitor(GetUserFilter(), new FileSystemReader());

                //visitor.Start += OutputEventMessage;
                //visitor.End += OutputEventMessage;
                //visitor.FileFinded += OutputEventMessage;
                //visitor.DirectoryFinded += OutputEventMessage;
                //visitor.FilteredFileFinded += OutputEventMessage;
                //visitor.FilteredDirectoryFinded += OutputEventMessage;

                foreach (var point in visitor.GetFilteredFsObjects(path))
                {
                    if (!string.IsNullOrEmpty(point)) Console.WriteLine(point);
                }
                Console.ReadKey();
            }
        }

        private static IFilter GetUserFilter()
        {
            bool parseSuccess;
            do
            {
                var filterProvider = new FilterProvider();
                var filtersNames = filterProvider.GetFiltersList().ToArray();
                Console.WriteLine("Press Enter for using default filter or choose filter from list:");
                for (var i = 0; i < filtersNames.Length; i++)
                {
                    var p = filtersNames[i];
                    Console.WriteLine($"{i}. {p}");
                }
                var filterString = Console.ReadLine();

                if (string.IsNullOrEmpty(filterString))
                    return filterProvider.GetDefaultFilter();
                if (!(parseSuccess = int.TryParse(filterString, out var filterNum)) || filterNum < 0 ||
                    filterNum >= filtersNames.Length)
                {
                    Console.WriteLine("Incorrect input");
                }
                else
                {
                    Console.WriteLine("Input filter pattern or press Enter...");
                    var pattern = Console.ReadLine();
                    return filterProvider.GetFilter(filtersNames[filterNum], pattern);
                }
            } while (!parseSuccess);
            return null;
        }

        private static void OutputEventMessage(object sender, FsVisitorEventArgs e)
        {
            Console.WriteLine($"Event invoked: {e.Message}");
        }


    }
}
